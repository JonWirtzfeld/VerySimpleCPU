// counter counts operation addresses for simple cpu
module counter( input [2:0] ir,
					 input clk, ld, inc, clr,
					 output reg [4:0] address);

always @(negedge clk) //decide next state on falling edge
begin
	if(ld == 1)
	begin
		address<={1'b1,ir,1'b0};
	end
	if(inc == 1)
	begin
		address<=address+1;
	end
	if(clr == 1)
	begin
		address<=5'b00000;
	end
	
end

endmodule
//Jon Wirtzfeld
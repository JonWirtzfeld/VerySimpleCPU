// Data Register 
// Module recieves 8-bit data from bus
// and sends 8-bit data to bus

module DR (clk, DRin, DRload, DRout);
	input [7:0] DRin;
	input clk, DRload;
	output [7:0] DRout;
	reg [7:0] DRout;
	
always@ (posedge clk)
	begin
		if (DRload == 1)		// if DRload signal is 1
			DRout = DRin[7:0];	// set DR value to new value
	end
	
endmodule
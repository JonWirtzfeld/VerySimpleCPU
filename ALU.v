// Arithmatic Logic Unit
// This module preforms addition and subtraction
// between the data register and the current
// value stored in the accumulator

module ALU (clk, ALUin, AC, ADD, SUB, ALUout);
	input [7:0] ALUin, AC;
	input clk, ADD, SUB;
	output reg [7:0] ALUout;

always @ (posedge clk)
	begin
		if (ADD == 1)						// check for addition
			begin
				ALUout <= AC + {3'b000, ALUin[4:0]};	// add accumlator to data register
			end
		else if (SUB == 1)				// else subtraction
			begin
				ALUout = AC - {3'b000, ALUin[4:0]};
			end								// subtract data register from accumulator
	end
	
endmodule
//Matthew Stluka



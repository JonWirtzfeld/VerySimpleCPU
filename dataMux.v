module dataMux(     input [7:0] memoryIn, DRIn, ACIn,
						  input [4:0] PCIn,
						  input memorySignal, PCSignal, DRSignal, ACSignal,
					     output reg [7:0] out);

always @(memorySignal or PCSignal or DRSignal)
begin
	if(memorySignal == 1)
		begin
			out <= memoryIn;
		end
	else if(PCSignal == 1)
		begin
			out <= {3'b0,PCIn};
		end
	else if(DRSignal == 1)
		begin
			out <= DRIn;
		end
	else if(ACSignal == 1)
		begin
			out <= ACIn;
		end
	else 
		begin 
			out <= memoryIn;
		end
end

endmodule
//Jon Wirtzfeld


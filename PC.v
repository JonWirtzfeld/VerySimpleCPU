// Program counter
// Module sets Program counter to the
// next instruction in the memory

module PC (clk, PCin, PCload, PCinc, PCout);
	input [7:0] PCin;
	input clk, PCload, PCinc;
	output [4:0] PCout;
	reg [4:0] PCout;
	
always@ (posedge clk)
	begin
		if (PCload == 1)		// if PCload signal is 1
			PCout = PCin[4:0];	// set PC address to new address
		else if (PCinc == 1)	// else if PCinc signal set to 1
			PCout = PCout + 1;	// PC address increased by 1
	end
	
endmodule
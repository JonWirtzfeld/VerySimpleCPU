// recieves and sends instruction bits

module IR (clk, IRin, IRload, IRout);
	input [7:0] IRin;
	input clk, IRload;
	output [2:0] IRout;
	reg [2:0] IRout;
	
always @ (posedge clk)
	begin
		if (IRload == 1)				// if IRload signal is 1
			IRout = IRin [7:5];		// set IR bits
	end

endmodule
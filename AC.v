// Accumulator
// this register keeps a running accumulation
// from the ALU

module AC (clk, ACin, ACload, ACMemIn, ACMemLoad, ACout);
	input [7:0] ACin, ACMemIn;
	input clk, ACload, ACMemLoad;
	output reg [7:0] ACout;

always @ (negedge clk)
	begin
		if (ACload == 1)			// check for load signal
			ACout = ACin;			// load value to to accumulator
		else if (ACMemLoad == 1)		// check for increment value
			ACout = ACMemIn;		// increment current value in accumulator by 1
	end

endmodule
//Matthew Stluka


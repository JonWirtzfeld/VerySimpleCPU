module clkDiv (clk, clkOut);
	input clk;
	output reg clkOut;
	reg [3:0]  i = 4'b0000;

always @ (negedge clk)
begin
		if (i < 5)
			begin
				i <= i+1;
			end
	   else if (i == 5)
			begin
				if (clkOut == 0)
					begin
						clkOut <= 1;
					end
				else
					begin
						clkOut <= 0;
					end
				i = 0;
			end
end

endmodule 
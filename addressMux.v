module addressMux(   input [4:0] AR, SP,
							input [7:0] DR,
						  input ARSignal, SPSignal, DRSignal,
					     output [4:0] out);

assign out[0] = (AR[0] & ARSignal)||(SP[0] & SPSignal) || (DR[0] & DRSignal); 
assign out[1] = (AR[1] & ARSignal)||(SP[1] & SPSignal) || (DR[1] & DRSignal);
assign out[2] = (AR[2] & ARSignal)||(SP[2] & SPSignal) || (DR[2] & DRSignal);
assign out[3] = (AR[3] & ARSignal)||(SP[3] & SPSignal) || (DR[3] & DRSignal);
assign out[4] = (AR[4] & ARSignal)||(SP[4] & SPSignal) || (DR[4] & DRSignal);

endmodule
//Jon Wirtzfeld
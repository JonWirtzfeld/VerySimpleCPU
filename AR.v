// Address Register
// Module will set last 5 bits of incoming bus
// if the ADload signal is set to 1

module AR (clk, ARin, ARload, ARout);
	input [7:0] ARin;
	input clk, ARload;
	output [4:0] ARout;
	reg [4:0] ARout;
	
always@ (posedge clk)
	begin
		if (ARload == 1)		// if load signal is 1 then 
			ARout = ARin[4:0];	// set register to new address
	end
	
endmodule
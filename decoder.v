// counter counts operation addresses for simple cpu
module decoder( input [4:0] address,
					 output fetch1, fetch2, fetch3, add1, add2, 
								subt1, subt2, psha1, psha2, pulla1,
								pulla2, load, store, jump);

assign fetch1 = (~address[4] & ~address[3] & ~address[2] & ~address[1] & ~address[0]); //00000
assign fetch2 = (~address[4] & ~address[3] & ~address[2] & ~address[1] & address[0]);  //00001
assign fetch3 = (~address[4] & ~address[3] & ~address[2] & address[1] & ~address[0]);  //00010
assign add1   = (address[4] & ~address[3] & ~address[2] & ~address[1] & ~address[0]);  //10000
assign add2   = (address[4] & ~address[3] & ~address[2] & ~address[1] & address[0]);   //10001
assign subt1  = (address[4] & ~address[3] & ~address[2] & address[1] & ~address[0]);   //10010
assign subt2  = (address[4] & ~address[3] & ~address[2] & address[1] & address[0]);    //10011
assign psha1  = (address[4] & ~address[3] & address[2] & ~address[1] & ~address[0]);   //10100
assign psha2  = (address[4] & ~address[3] & address[2] & ~address[1] & address[0]);    //10101
assign pulla1 = (address[4] & ~address[3] & address[2] & address[1] & ~address[0]);    //10110
assign pulla2 = (address[4] & ~address[3] & address[2] & address[1] & address[0]);     //10111
assign load   = (address[4] & address[3] & ~address[2] & ~address[1] & ~address[0]);   //11000
assign store  = (address[4] & address[3] & ~address[2] & address[1] & ~address[0]);    //11010
assign jump   = (address[4] & address[3] & address[2] & ~address[1] & ~address[0]);     //11100

endmodule
//Jon Wirtzfeld
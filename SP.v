// Stack Pointer
// Module adjust Stack Pointer address
// of to facilitate push / pull data

module SP (clk, SPin, SPload, SPinc, SPdec, SPout);
	input [7:0] SPin;
	input clk, SPload, SPinc, SPdec;
	output [4:0] SPout;
	reg [4:0] SPout = 5'b11111;
	
always@ (posedge clk)
	begin
		if (SPload == 1)			// if SPload is 1
			SPout = SPin;			// set SP address
		else if (SPinc == 1)		// if SPinc signal is 1
			SPout = SPout + 1;	// increase SP address by 1
		else if (SPdec == 1)		// else if SPdec signal set to 1
			SPout = SPout - 1;	// decrease SP address by 1
	end
	
endmodule
//Matthew Stluka

